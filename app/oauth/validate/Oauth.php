<?php

namespace app\oauth\validate;

use think\Validate;

class Oauth extends Validate
{
    protected $rule = [
        'username' => 'require|max:80',
        'password' => 'require|max:80',
        'old_password' => 'require|max:80',
        'new_password' => 'require|max:80',
    ];

    protected $message = [
        'username.require' => 'username必须',
        'password.require' => 'password必须',
        'old_password.require' => 'old_password必须',
        'new_password.require' => 'new_password必须',
    ];

    protected $scene = [
        'login' => ['username', 'password'],
        'change_password' => ['username','old_password', 'new_password'],
    ];
}