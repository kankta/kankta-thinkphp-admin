<?php
declare (strict_types=1);

namespace app\oauth\middleware;

use app\oauth\service\AuthService;
use app\oauth\service\TokenService;

class TokenCheck
{
    /**
     * @param $request
     * @param \Closure $next
     * @return mixed|\think\response\Json
     */
    public function handle($request, \Closure $next)
    {
        $header_token = $request->header('token');

        if (!$header_token) {
            return JsonData(FAILED_CODE_TOKEN, "token不能为空");
        }

        $res = explode('_', $header_token);

        $token = $res[0];
        $user_id = $res[1];

        $TokenService = new TokenService();

        $admin = $TokenService->getAdminByToken($user_id, $token);

        if (!$admin) {
            return JsonData(FAILED_CODE_TOKEN, "token错误");
        }

        AuthService::setUser($admin);

        if (!$TokenService->checkVaildTime()) {
            return JsonData(FAILED_CODE_TOKEN, "token过期");
        }

        return $next($request);
    }
}
