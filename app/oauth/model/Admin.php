<?php
declare (strict_types=1);

namespace app\oauth\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class Admin extends Model
{
    protected $autoWriteTimestamp = 'datetime';
    protected $createTime = 'created_at';
    protected $updateTime = 'updated_at';

    public function getByUserName($username)
    {
        return Admin::where(['username' => $username, 'deleted_at' => null])->find();
    }
}
