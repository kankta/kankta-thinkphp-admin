<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

Route::post('login', 'oauth/login');
Route::post('register', 'oauth/register');
Route::get('installed', 'oauth/installed');

Route::group(function () {
    Route::post('change_password', 'oauth/changePassword');
    Route::get('logout', 'oauth/logout');
})->middleware(\app\oauth\middleware\TokenCheck::class);


