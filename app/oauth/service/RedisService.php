<?php

namespace app\oauth\service;

class RedisService
{
    static private $redis;
    //创建静态私有的变量保存该类对象 -- 一私
    static private $instance;

    //防止直接创建对象 -- 二私
    private function __construct()
    {
        self::$redis = new \Redis();
        self::$redis->connect(env('redis.host'), env('redis.port'));
    }

    //防止克隆对象 -- 三私
    private function __clone()
    {

    }

    //公有的静态方法 (调取这个类相当一个接口 ) -- 一公
    static public function getInstance()
    {
        //判断$instance是否是Uni的对象
        //没有则创建
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    static public function getRedis(){
        self::getInstance();
        return self::$redis;
    }
}