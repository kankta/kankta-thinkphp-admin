<?php
declare (strict_types=1);

namespace app\oauth\service;

use app\oauth\model\Admin;

class PasswordService
{
    /**
     * @param $password
     * @param $salt
     * @return string
     */
    public static function getPassWord($password, $salt)
    {
        $sha1 = sha1($password);
        return md5($sha1 . $salt);
    }
}