<?php

namespace app\oauth\service;

class AuthService
{
    //参数
    static private $user;

    /**
     * @param $user
     * @return AuthService
     */
    static public function setUser($user)
    {
        self::$user = $user;
    }

    static public function user()
    {
        return self::$user;
    }

    static public function userId()
    {
        return self::$user['id'];
    }

    static public function userName()
    {
        return self::$user['username'];
    }
}
