<?php
declare (strict_types=1);

namespace app\oauth\service;
use app\oauth\service\ExceptionService;

class ValidateService
{
    public static function check($obj_name, $scene, $data)
    {
        $obj = "app\\oauth\\validate\\" . $obj_name;
        $validate = new $obj();

        $return = $validate->scene($scene)
            ->check($data);

        if (!$return) {
            throw new ExceptionService($validate->getError(), FAILED_CODE_VALIDATE);
        }
    }
}