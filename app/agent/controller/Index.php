<?php
declare (strict_types=1);

namespace app\agent\controller;

use think\Request;

class Index
{
    public function index(Request $request)
    {
        $data['post']=$request->post();
        $data['get']=$request->get();
        $data['header']=$request->header();

        return JsonData(SUCCESS_CODE, "成功",$data);
    }
}
