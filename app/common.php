<?php
// 应用公共文件
function JsonData($code = 0, $msg = '', $data = [])
{
    $return_data['code'] = $code;
    $return_data['msg'] = $msg;
    $return_data['data'] = $data;
    return json()->data($return_data);
}

define('SUCCESS_CODE',0);
define('FAILED_CODE_VALIDATE',10001); //参数验证错误
define('FAILED_CODE_TOKEN',9999); //登录token错误