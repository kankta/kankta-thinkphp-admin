## XIANG快速开发后台管理系统后端接口
- 采用thinkphp6.0
- 需要安装redis,php7.4,mysql8.0


### 功能介绍
thinkphp快速开发框架后台后端接口

### 已开发功能
#### 管理员模块
- 项目安装
- 管理员登录,修改密码,退出登录,可设定登录token时效,管理员初始化密码

### 接口文档地址
https://docs.apipost.cn/preview/4f573895ece9bd9f/b7f1c79aaa2daa5b

### 数据库
数据库文件在sql目录下

### 联系方式
- e-mail address : 1124557724@qq.com
- wechat : xuyaoxiang91